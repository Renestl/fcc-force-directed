# FCC Force Directed Graph

[Live Demo](http://nasty-cart.surge.sh/)

## Description
This project utilizes d3.js to display a force directed graph that depicts which countries share borders.

## Uses
* HTML
* CSS
* JavaScript
* D3.js

## About
This project was written as a requirement in the [Data Visualization Projects](https://www.freecodecamp.org/challenges/show-national-contiguity-with-a-force-directed-graph)
 for [FreeCodeCamp](https://www.freecodecamp.org)


## License
[MIT](https://tldrlegal.com/license/mit-license) &copy; 2018 [Jennifer Currie](https://github.com/Renestl)