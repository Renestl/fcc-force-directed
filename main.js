document.addEventListener("DOMContentLoaded", function(event) {

	var url = "https://raw.githubusercontent.com/DealPete/forceDirected/master/countries.json";

	var margin = {top: 20, right: 10, bottom: 10, left: 20},
				fullWidth = 860,
				fullHeight = 575;

	// used for ranges of the scales
	var width = fullWidth - margin.right - margin.left;
	var height = fullHeight - margin.top - margin.bottom;

	// used for forces
	var link_distance = 25;
	var link_strength = 1;

	fetch(url)
		.then(function(response) {
			return response.json();
		})
		.then(function(data) {

			var nodes = data.nodes;
			var links = data.links;

			console.log(nodes);
			console.log(links);

			function getFlag(node) {
				var flag = `flag flag-${node.code}`
				return flag;
			}

			var chart = d3.select(".chart")
				.attr("width", width)
				.attr("height", height);

			// set up simulation
			// affects every node
			var simulation = d3.forceSimulation()
				.nodes(nodes);

			simulation
			//  electrostatic effects, which makes the graph feel organic and natural as the nodes affect each other
			.force("charge", d3.forceManyBody().strength(-5))
			// translates all nodes to visually move them into the center of the svg element
			.force("center", d3.forceCenter(width / 2, height / 2));
			
			const nodeElements = chart.append('g')
				.attr("class", "nodes")
				.selectAll('circle')
				.data(nodes)
				.enter()
				.append('circle')
				.attr('r', 10)
				.attr('class', getFlag)

			function tickActions() {
				//update positions to reflect node updates on each tick of the simulation
				// also constrains to box
				nodeElements
					.attr("cx", function(d) { return d.x = Math.max(10, Math.min(width-10, d.x)); })
					.attr("cy", function(d) { return d.y = Math.max(10, Math.min(height-10, d.y)); })

				//update link positions 
				//simply tells one end of the line to follow one node around
				//and the other end of the line to follow the other node around
				linkElements
					.attr('x1', function(d) { return d.source.x })
					.attr('y1', function(d) { return d.source.y })
					.attr('x2', function(d) { return d.target.x })
					.attr('y2', function(d) { return d.target.y })
			}

			simulation.on("tick", tickActions);

			// Create link force
			// need id accessor to use named sources and targets

			var link_force = d3.forceLink(links)
				.id(function(d) { return d.index; })
				.distance(function(d) { return link_distance; })
				.strength(function(d) { return link_strength });

			simulation.force("links", link_force)

			// draw lines for the links
			var linkElements = chart.append("g")
					.attr("class", "link")
				.selectAll("line")
				.data(links)
				.enter()
				.append("line")

			// drag handler with d3.drag()
			var drag_handler = d3.drag()
				.on("start", drag_start)
				.on("drag", drag_drag)
				.on("end", drag_end);

			function drag_start(d) {
				// alphaTarget controls how quickly the simulation returns to equilibrium
				if (!d3.event.active) simulation.alphaTarget(0.3).restart();
				d.fx = d.x;
				d.fy = d.y;
			}
			function drag_drag(d) {
				d.fx = d3.event.x;
				d.fy = d3.event.y;
			}
			function drag_end(d) {
				if (!d3.event.active) simulation.alphaTarget(0);
				d.fx = null;
				d.fy = null;
			}

			drag_handler(nodeElements);

		// end data then function
		});
// end document.addEventListener
});